import { ChangeEvent, FormEvent, useCallback, useState } from 'react';
import { ValidationErrorItem } from 'joi';
import { FormDataRecord, IFormProps } from './Form.types';

const useForm = (
  onSubmit: IFormProps['onSubmit'],
  onValidate?: IFormProps['onValidate'],
) => {
  const [errors, setErrors] = useState<ValidationErrorItem[]>([{} as ValidationErrorItem]);
  const onSubmitHandler = useCallback((event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const result = getObjectFromFormData(new FormData(event.currentTarget));
    const validationErrors = onValidate?.(result) || [];

    setErrors(validationErrors);

    if (!validationErrors.length) {
      onSubmit?.(result);
    }
  }, [onSubmit, onValidate, setErrors]);
  const onChangeHandler = useCallback((event: ChangeEvent<HTMLFormElement>) => {
    if (!onValidate) return [];

    const formData = getObjectFromFormData(new FormData(event.currentTarget))

    return setErrors(onValidate(formData) || []);
  }, [onValidate, setErrors]);

  return { errors, onChangeHandler, onSubmitHandler };
};

export const getObjectFromFormData = (formData: FormData) => {
  const result: FormDataRecord = {};

  for (const [name, value] of formData.entries()) {
    switch (typeof value) {
      case 'string':
      case 'number':
      case 'boolean':
        result[name] = value;
        break;
      default:
        continue;
    }
  }

  return result;
}

export default useForm;
