import { render, fireEvent } from '@testing-library/react';
import Form from '.';

const INPUT_TEST_ID = 'Input';
const BUTTON_ID = 'Button';
const FORM_TEST_ID = 'Form';

describe('Form - positive', () => {
  const onSubmit = jest.fn();

  test('renders a form element', () => {
    const { getByTestId } = render(<Form onSubmit={onSubmit} />);
    const form = getByTestId(FORM_TEST_ID);

    expect(form).toBeDefined();
    expect(form).toMatchSnapshot();
  });

  test('renders a form element with children', () => {
    const { getByTestId } = render(
      <Form onSubmit={onSubmit}>
        <input type="text" name="login" data-testid={INPUT_TEST_ID} />
      </Form>
    );
    const input = getByTestId(INPUT_TEST_ID);
    const button = getByTestId(BUTTON_ID);

    expect(input).toBeDefined();
    expect(button).toBeDefined();
  });
});

describe('Form - negative', () => {
  const onSubmit = jest.fn();

  test('renders a form element', () => {
    const { getByTestId } = render(<Form onSubmit={onSubmit} />);
    const form = getByTestId(FORM_TEST_ID);

    expect(form).toBeDefined();
    expect(form).toMatchSnapshot();
  });

  test('renders form with a disabled button', () => {
    const { getByTestId } = render(
      <Form onSubmit={onSubmit} >
        <input type="text" name="login" />
      </Form>
    );
    const button = getByTestId(BUTTON_ID);

    expect(button).toBeDefined();
    expect((button as HTMLButtonElement)?.disabled).toBeTruthy();
  });

  test('calls "onValidate" on form submit', () => {
    const onSubmitWithDelay = jest.fn().mockReturnValue([
      {
        message: '"login" must be an email',
        path: [ 'login' ],
        type: 'email.base',
        context: { value: '%%', key: 'login', label: 'login' },
      }
    ]);
    const { getByTestId } = render(
      <Form
        onSubmit={onSubmit}
        onValidate={onSubmitWithDelay}
      >
          <input type="text" name="login" value="%%" />
      </Form>
    );
    const button = getByTestId(BUTTON_ID);

    expect(button).toBeDefined();

    fireEvent(button, new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
    }));

    expect(onSubmitWithDelay).not.toBeCalled();
  });

  test('calls "onValidate" on form submit', () => {
    const onSubmit = jest.fn();
    const { getByTestId } = render(
      <Form
        onSubmit={onSubmit}
      >
          <input type="text" name="login" value="%%" />
      </Form>
    );
    const button = getByTestId(BUTTON_ID);

    expect(button).toBeDefined();

    fireEvent(button, new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
    }));

    expect(onSubmit).not.toBeCalled();
  });
});
