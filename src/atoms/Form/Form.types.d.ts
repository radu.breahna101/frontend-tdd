import { ValidationErrorItem } from 'joi';

export type FormDataRecord = Record<string, FormDataEntryValue & number | string | boolean>;

export interface IFormProps {
  onSubmit: (form: FormDataRecord) => void;
  onValidate?: (form: FormDataRecord) => ValidationErrorItem[];
}
