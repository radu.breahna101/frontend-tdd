import { renderHook, render } from '@testing-library/react';
import useForm from './useForm';
import { waitFor } from '@testing-library/react';


describe('useForm', () => {
  it('should return { errors, onChangeHandler, onSubmitHandler }', () => {
    const { result } = renderHook(useForm);

    expect(result.current.errors).toEqual([{}]);
    expect(result.current.onChangeHandler).toBeInstanceOf(Function);
    expect(result.current.onSubmitHandler).toBeInstanceOf(Function);
  });

  it('should call onSubmit', () => {
    const onSubmit = jest.fn();
    const { result } = renderHook(() => useForm(onSubmit));
    const { getByTestId } = render(
      <form data-testid="Form">
        <input name="prop" value="prop value" />
      </form>
    );
    const form = getByTestId('Form');

    result.current.onSubmitHandler({ currentTarget: form, preventDefault: jest.fn() } as any);

    expect(onSubmit).toBeCalledTimes(1);
    expect(onSubmit).toHaveBeenCalledWith({ prop: 'prop value' });
  });

  it('should call onValidate', () => {
    const onSubmit = jest.fn();
    const onValidate = jest.fn();
    const { result } = renderHook(() => useForm(onSubmit, onValidate));
    const { getByTestId } = render(
      <form data-testid="Form">
        <input name="prop" value="prop value" />
      </form>
    );
    const form = getByTestId('Form');

    result.current.onSubmitHandler({ currentTarget: form, preventDefault: jest.fn() } as any);
    result.current.onChangeHandler({ currentTarget: form, preventDefault: jest.fn() } as any);

    expect(onValidate).toBeCalledTimes(2);
  });

  it('"errors" should contain one Joi error', async () => {
    const  preventDefault = jest.fn();
    const errors = [{ message: 'Joi error' }];
    const onSubmit = jest.fn();
    const onValidate = jest.fn().mockReturnValue(errors);
    const { result } = renderHook(() => useForm(onSubmit, onValidate));
    const { getByTestId } = render(
      <form data-testid="Form">
        <input name="prop" value=",l," />
      </form>
    );
    const form = getByTestId('Form');

    result.current.onSubmitHandler({ currentTarget: form, preventDefault } as any);

    expect(onValidate).toBeCalledTimes(1);
    expect(preventDefault).toBeCalledTimes(1);

    await waitFor(() => !!result.current.errors[0].message)

    expect(result.current.errors[0].message).toEqual(errors[0].message);
  });
});
