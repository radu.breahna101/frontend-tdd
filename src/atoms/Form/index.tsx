import { PropsWithChildren, ReactElement } from 'react';
import { IFormProps } from './Form.types';
import useForm from './useForm';

const Form = ({ children, onSubmit, onValidate }: PropsWithChildren<IFormProps>): ReactElement => {
  const { onSubmitHandler, onChangeHandler, errors } = useForm(onSubmit, onValidate);

  return (
    <div className="w-full max-w-xs">
      <form
        data-testid="Form"
        className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4"
        onSubmit={errors.length ? () => null : onSubmitHandler}
        onChange={onChangeHandler}
      >
        {children}
        <button
          data-testid="Button"
          disabled={!!errors.length}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit"
        >
          Submit
        </button>
      </form>
    </div>
  );
};

export default Form;
