# TDD example

This project is just an example that demonstrates one of the TDD development process approaches.

I will break the components into atoms and molecules so that it is clear where to apply integration tests.

### Configuration
- npm i
- npm i -g vite

### How to start
- npm run dev

### User story
Let's imagine we have a story to build a sign-in form for the site example.com.

**As a user**, I want to be able to enter my login and password. Both fields **should** be validated properly and in case there are any errors, **I should** be alerted to the existing errors and **should** be blocked from proceeding to submit the form. In case all the data is fine, **I should** be able to click the Submit button and once my request is triggered **I shouldn't** be able to click the Submit button oce again. If the request was successfully processed **I should** see a greeting message instead of form. In case there are any errors on the server side, **I should** receive a comprehensive message about it. In any case, after the request is processed the Submit button should become clickable.

#### ACs
- **Login** must be an email, required
- **Password** must be between 6 and 8 symbols inclusively (a-z|A-Z|0-9)
- **Submit** button should be disabled once there's is an error in one of the fields. Should be enabled after the error disappears
- **Error message** - should appear when there's an error, and disappear when the field is filled in correctly
- **Greeting message** should appear once the form is successfully processed

#### Testing plan
- **Login field**
```diff
+ Happy flow
  - On Focus or On Blur event shouldn't cause a validation error. Field should be enabled.
  - Enter "example@gmail.com" - No errors. The Submit button should be enabled. The field is enabled.
- Unhappy flow
  - Enter "example.com" and move focus - An error should appear explaining that the field is out of character range
  - Leave the field empty and move the focus - An error should appear explaining that the field is out of character range
```

- **Password field**
```diff
+ Happy flow
  - On Focus or On Blur event shouldn't cause a validation error. Field should be enabled.
  - Enter 6, 7 or 8 symbols (a-z|A-Z|0-9) - No errors. The Submit button should be enabled. The field is enabled.
- Unhappy flow
  - Enter 5 or 9 symbols and move focus - An error should appear explaining that the field is out of character range
  - Enter 6, 7 or 8 characters that don't match the pattern [a-z|A-Z|0-9]{6,8}
```

- **Submit button**
```diff
+ Happy flow
  1) The fields are correctly filled in and we're pressing Submit - the request should be triggered and the button should be disabled while it's being processed.
  2) Button should be disabled once the processing is finished
  3) Greeting message should be shown
- Unhappy flow
  - Fields are empty or with incorrect input. The Submit button should be disabled.
  - Fields are filled in correctly. After the form is submitted server fails with status code = 500 - the submit button should be enabled after the request is processed
```

### What's next?
- We have to prepare a plan. First of all I would suggest to separate business logic from views. Or at least to separate functionality from the view component. Do it more granular so it could be reusable and separately testable
- In case we've separated the view from the functionality, we can mock the functionality and simply test all the views with snapshots + interactions (Handler calls like onClick, onInput, onChange, onFocus and so on).
- Start with tests. Write granular tests for each of the units

While I'm adding tests, I create corresponding files like Form.constants.ts, Form.types.d.ts and so on. So far, they're just placeholders...
